#!/usr/bin/env ruby

require 'nokogiri'
require 'open-uri'
require 'table_print'

SOURCE = 'https://h41369.www4.hp.com/pps-offers.php'

doc = Nokogiri::HTML(open(SOURCE))
class Computer
  attr_accessor :description, :price, :sale_price, :bonus, :model

  def has_ecc?
    !!(description =~ /ecc/i)
  end

  def has_ssd?
    !!(description =~ /ssd/i)
  end

  def real_price
    sale_price || price
  end

  def self.parse_row(source)
    v = new
    v.description = source.children[0].text
    v.model = source.children[1].text
    v.price = parse_price source.children[2].text
    v.sale_price = parse_price source.children[3].text
    v.bonus = source.children[4].text
    v
  end

  def self.parse_price val
    x = val.gsub(/\D/,'')
    x.empty? ? nil : x.to_i
  end
end
rows = doc.css('#workstations table tr')
computers = rows[1 .. -1].map{|x| Computer.parse_row(x) }.sort_by(&:real_price)
interesting_computers = []
interesting_computers << computers.first
interesting_computers << computers.detect {|x| x.has_ecc? }
interesting_computers << computers.detect {|x| x.has_ssd? }
interesting_computers << computers.detect {|x| x.has_ssd? && x.has_ecc? }
tp interesting_computers
